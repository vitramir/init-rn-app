import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

module.exports = async (
  config: { projectDir: string },
  params: {
    projectName: string;
    bundleId: string;
    iosBundleId: string;
    androidBundleId: string;
  }
) => {
  console.log(`Set bundle id`);

  const progConf = { base: ".", cwd: config.projectDir };

  const iosBundleId = params?.iosBundleId || params?.bundleId;
  if (!iosBundleId) {
    throw "iOS bundle identifier not provided";
  }
  await Promisify(
    gulp
      .src("./ios/**/project.pbxproj", progConf)
      .pipe(
        inject.replace(
          `org\\.reactjs\\.native\\.example\\.\\$\\(PRODUCT_NAME:rfc1034identifier\\)`,
          `${iosBundleId}`
        )
      )
      .pipe(gulp.dest("./"))
  );

  const androidBundleId = params?.androidBundleId || params?.bundleId;
  if (!androidBundleId) {
    throw "Android bundle identifier not provided";
  }
  await Promisify(
    gulp
      .src("./android/app/build.gradle", progConf)
      .pipe(inject.replace(`com.${params.projectName}`, `${androidBundleId}`))
      .pipe(gulp.dest("./"))
  );

  console.log("Done!");
};
