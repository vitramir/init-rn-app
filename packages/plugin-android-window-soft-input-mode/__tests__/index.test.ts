import ncp from 'ncp';
import rimraf from 'rimraf';
import path from 'path';
import fs from 'fs';
import pify from 'pify';

const plugin = require('../src');

it('test plugin', async () => {
  const templatePath = path.resolve(__dirname, 'templateProject');
  const tmpPath = path.resolve(__dirname, '_tmp');

  rimraf.sync(tmpPath); //remove tmp dir
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  //apply plugin
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      mode: 'NEW_MODE',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/AndroidManifest.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<manifest xmlns:android=\\"http://schemas.android.com/apk/res/android\\"
      package=\\"com.package\\">
        <uses-permission android:name=\\"android.permission.INTERNET\\" />
            <application
          android:name=\\".MainApplication\\"
          android:label=\\"@string/app_name\\"
          android:icon=\\"@mipmap/ic_launcher\\"
          android:roundIcon=\\"@mipmap/ic_launcher_round\\"
          android:allowBackup=\\"false\\"
          android:theme=\\"@style/AppTheme\\">
        <activity
            android:name=\\".MainActivity\\"
            android:label=\\"@string/app_name\\"
            android:configChanges=\\"keyboard|keyboardHidden|orientation|screenSize\\"
            aandroid:windowSoftInputMode=\\"NEW_MODE\\">
            <intent-filter>
                <action android:name=\\"android.intent.action.MAIN\\" />
                <category android:name=\\"android.intent.category.LAUNCHER\\" />
            </intent-filter>
          </activity>
          <activity android:name=\\"com.facebook.react.devsupport.DevSettingsActivity\\" />
        </application>

    </manifest>
    "
  `);
});
