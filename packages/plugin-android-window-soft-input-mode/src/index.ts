import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: { mode: string }
) => {
  console.log(`Setup android window soft input mode`);

  const progConf = { base: '.', cwd: config.projectDir };

  await Promisify(
    gulp
      .src('./android/app/src/main/AndroidManifest.xml', progConf)
      .pipe(
        inject.replace(
          'android:windowSoftInputMode="adjustResize"',
          `android:windowSoftInputMode="${params.mode}"`
        )
      )
      .pipe(gulp.dest('./'))
  );

  console.log('Done!');
};
