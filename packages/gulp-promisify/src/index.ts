export default pipe => {
  return new Promise(function(resolve, reject) {
    pipe.on('error', reject).on('end', resolve);
  });
};
