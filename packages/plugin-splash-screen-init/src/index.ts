import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor'),
  path = require('path'),
  fs = require('fs'),
  rimraf = require('rimraf'),
  mkdirp = require('mkdirp');

module.exports = async (
  config: {
    projectDir: string;
  },
  params: {
    projectName: string;
    launchscreenPath: string;
    xibPath: string;
    imagesPath: string;
    backgroundColor: string;
  }
) => {
  const progConf = { base: '.', cwd: config.projectDir }; // Project path
  console.log('Loading splash-screen');

  await Promisify(
    gulp
      .src(
        `./android/app/src/main/java/com/${params.projectName}/MainActivity.java`,
        progConf
      )
      .pipe(
        inject.after(
          `package com.${params.projectName};\n`,
          'import android.os.Bundle;\nimport org.devio.rn.splashscreen.SplashScreen;\n'
        )
      )
      .pipe(
        inject.after(
          'public class MainActivity extends ReactActivity {',
          `\n@Override
      protected void onCreate(Bundle savedInstanceState) {
          SplashScreen.show(this);
          super.onCreate(savedInstanceState);
      }`
        )
      )
      .pipe(gulp.dest('./'))
  );

  params.backgroundColor &&
    fs.writeFileSync(
      `${config.projectDir}/android/app/src/main/res/values/colors.xml`,
      `<?xml version="1.0" encoding="utf-8"?>
<resources>
    <color name="primary_dark">#000000</color>
    <color name="background">${params.backgroundColor}</color>

</resources>
    `
    );

  params.backgroundColor &&
    (await Promisify(
      gulp
        .src(`./android/app/src/main/res/values/styles.xml`, progConf)
        .pipe(
          inject.after(
            `<style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">\n`,
            `<!-- <item name="android:background">@color/background</item> -->
        <item name="android:windowBackground">@color/background</item>
        <item name="android:colorBackground">@color/background</item>\n`
          )
        )

        .pipe(gulp.dest('./'))
    ));

  rimraf.sync(`${config.projectDir}/android/app/src/main/res/layout`);
  fs.mkdirSync(`${config.projectDir}/android/app/src/main/res/layout`);

  fs.writeFileSync(
    `${config.projectDir}/android/app/src/main/res/layout/launch_screen.xml`,
    `<?xml version="1.0" encoding="utf-8"?>
<FrameLayout
  xmlns:android="http://schemas.android.com/apk/res/android"
  android:layout_width="match_parent"
  android:layout_height="match_parent"
  android:orientation="vertical"
>
  <ImageView
    android:src="@drawable/launch_screen"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:scaleType="centerCrop"
  >
  </ImageView>
</FrameLayout>
`
  );

  //TODO: Copy launch_screen.png either in plugin or in init-rn-app-copy-files directory?

  params.launchscreenPath &&
    [
      'drawable-hdpi',
      'drawable-ldpi',
      'drawable-mdpi',
      'drawable-xhdpi',
      'drawable-xxhdpi',
      'drawable-xxxhdpi',
    ].forEach((dest) => {
      rimraf.sync(`${config.projectDir}/android/app/src/main/res/${dest}`);
      fs.mkdirSync(`${config.projectDir}/android/app/src/main/res/${dest}`);

      switch (dest) {
        case 'drawable-hdpi':
          fs.copyFileSync(
            `${params.launchscreenPath}/launch_screen@2x.png`,
            `${config.projectDir}/android/app/src/main/res/${dest}/launch_screen.png`
          );
          break;
        case 'drawable-xhdpi':
        case 'drawable-xxhdpi':
        case 'drawable-xxxhdpi':
          fs.copyFileSync(
            `${params.launchscreenPath}/launch_screen@3x.png`,
            `${config.projectDir}/android/app/src/main/res/${dest}/launch_screen.png`
          );
          break;
        default:
          fs.copyFileSync(
            `${params.launchscreenPath}/launch_screen.png`,
            `${config.projectDir}/android/app/src/main/res/${dest}/launch_screen.png`
          );
          break;
      }
      return null;
    });

  await Promisify(
    gulp
      .src(`./ios/${params.projectName}/AppDelegate.m`, progConf)
      .pipe(inject.prepend('#import "RNSplashScreen.h"\n'))
      .pipe(
        inject.after(
          `[self.window makeKeyAndVisible];`,
          `\n[RNSplashScreen show];\n`
        )
      )
      .pipe(gulp.dest('./'))
  );

  if (params.xibPath) {
    mkdirp.sync(`${config.projectDir}/ios/${params.projectName}/Base.lproj`);
    fs.copyFileSync(
      params.xibPath,
      `${config.projectDir}/ios/${params.projectName}/Base.lproj/LaunchScreen.xib`
    );
  }

  // detectImages -> copyImages (copyImages -> createJsons) -> ???

  const setScaleItem = (item) => {
    if (item.indexOf('@') == -1) return '1x';
    return item.slice(item.indexOf('@') + 1, item.length - 4);
  };

  if (params.imagesPath) {
    const files = fs.readdirSync(params.imagesPath, {});
    const nFiles = files.filter(
      (item) => item.slice(item.length - 3, item.length) === 'png'
    );

    /* --> detectingModule Start <-- */
    let uniqueFiles = {};
    nFiles.forEach((element) => {
      let rawName = element.slice(0, element.length - 4); // --> '.png' removed from file name
      if (['@2x', '@3x'].includes(rawName.slice(rawName.length - 3))) {
        /* --> removing scaling postfix <-- */
        rawName = rawName.slice(0, rawName.length - 3);
      }
      uniqueFiles[rawName] = [...(uniqueFiles[rawName] || []), element];
    });

    /* --> detectingModule End
      copyImages start
      files.entries -> mkdir key+'Splash.imageset' -> copyImages dir+value-> createJson by pattern <-- */

    Object.entries(uniqueFiles).forEach(async (value) => {
      rimraf.sync(
        `${config.projectDir}/ios/${params.projectName}/Images.xcassets/${value[0]}Splash.imageset`
      );
      mkdirp.sync(
        `${config.projectDir}/ios/${params.projectName}/Images.xcassets/${value[0]}Splash.imageset`
      );
      const images: any = value[1];
      typeof images == 'object' &&
        images.forEach(async (element) => {
          fs.copyFileSync(
            `${params.imagesPath}/${element}`,
            `${config.projectDir}/ios/${params.projectName}/Images.xcassets/${value[0]}Splash.imageset/${element}`
          );
        });
      const json =
        `
                {
          "images" : [` +
        images.map(
          (item) => `{
              "idiom" : "universal",
              "filename" : "${item}",
              "scale" : "${setScaleItem(item)}"
            }`
        ) +
        `],
          "info" : {
            "version" : 1,
            "author" : "xcode"
          }
        }
        `;
      fs.writeFileSync(
        `${config.projectDir}/ios/${params.projectName}/Images.xcassets/${value[0]}Splash.imageset/Contents.json`,
        json
      );
    });
  }

  // copyImages

  console.log('Done add SplashScreen!');
};
