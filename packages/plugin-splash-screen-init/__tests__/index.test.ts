import ncp from 'ncp';
import rimraf from 'rimraf';
import path from 'path';
import fs from 'fs';
import pify from 'pify';
import tmp from 'tmp';

const plugin = require('../src');
const templatePath = path.resolve(__dirname, 'templateProject');
let tmpPath: string;
let tmpObj;

beforeAll(() => {
  tmpObj = tmp.dirSync(undefined);
  tmpPath = tmpObj.name;
});

afterAll(() => {
  rimraf.sync(tmpPath);
});

it('test launch_screen_Android', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      launchscreenPath:
        './packages/plugin-splash-screen-init/__tests__/splashScreen/launchScreen',
    }
  );

  [
    'drawable-hdpi',
    'drawable-ldpi',
    'drawable-mdpi',
    'drawable-xhdpi',
    'drawable-xxhdpi',
    'drawable-xxxhdpi',
  ].forEach((dest) => {
    expect(
      fs.existsSync(
        path.resolve(
          tmpPath,
          `./android/app/src/main/res/${dest}/launch_screen.png`
        )
      )
    ).toBeTruthy();
  });
});

it('test MainActivity_Android', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(
          tmpPath,
          './android/app/src/main/java/com/templateProject/MainActivity.java'
        )
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "package com.templateProject;
    import android.os.Bundle;
    import org.devio.rn.splashscreen.SplashScreen;
    import android.os.Bundle;
    import org.devio.rn.splashscreen.SplashScreen;

    import com.facebook.react.ReactActivity;

    public class MainActivity extends ReactActivity {
    @Override
          protected void onCreate(Bundle savedInstanceState) {
              SplashScreen.show(this);
              super.onCreate(savedInstanceState);
          }
    @Override
          protected void onCreate(Bundle savedInstanceState) {
              SplashScreen.show(this);
              super.onCreate(savedInstanceState);
          }

      /**
       * Returns the name of the main component registered from JavaScript. This is used to schedule
       * rendering of the component.
       */
      @Override
      protected String getMainComponentName() {
        return \\"templateProject\\";
      }
    }
    "
  `);
});

it('test layout_Android', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(
          tmpPath,
          './android/app/src/main/res/layout/launch_screen.xml'
        )
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>
    <FrameLayout
      xmlns:android=\\"http://schemas.android.com/apk/res/android\\"
      android:layout_width=\\"match_parent\\"
      android:layout_height=\\"match_parent\\"
      android:orientation=\\"vertical\\"
    >
      <ImageView
        android:src=\\"@drawable/launch_screen\\"
        android:layout_width=\\"match_parent\\"
        android:layout_height=\\"match_parent\\"
        android:scaleType=\\"centerCrop\\"
      >
      </ImageView>
    </FrameLayout>
    "
  `);
});

it('test colors_Android', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      backgroundColor: '#212329',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/res/values/colors.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<?xml version=\\"1.0\\" encoding=\\"utf-8\\"?>
    <resources>
        <color name=\\"primary_dark\\">#000000</color>
        <color name=\\"background\\">#212329</color>

    </resources>
        "
  `);
});

it('test styles', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      backgroundColor: '#212329',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/res/values/styles.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<resources>

        <!-- Base application theme. -->
        <style name=\\"AppTheme\\" parent=\\"Theme.AppCompat.Light.NoActionBar\\">
    <!-- <item name=\\"android:background\\">@color/background</item> -->
            <item name=\\"android:windowBackground\\">@color/background</item>
            <item name=\\"android:colorBackground\\">@color/background</item>
            <!-- Customize your theme here. -->
            <item name=\\"android:textColor\\">#000000</item>
        </style>

    </resources>
    "
  `);
});

it('test LaunchScreenXib_Ios', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      xibPath:
        './packages/plugin-splash-screen-init/__tests__/splashScreen/LaunchScreen.xib',
    }
  );

  expect(
    fs.existsSync(
      path.resolve(tmpPath, `./ios/templateProject/Base.lproj/LaunchScreen.xib`)
    )
  ).toBeTruthy();
});

it('test AppDelegate_Ios', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      xibPath:
        './packages/plugin-splash-screen-init/__tests__/splashScreen/LaunchScreen.xib',
      imagesPath:
        './packages/plugin-splash-screen-init/__tests__/splashScreen/images',
    }
  );
  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './ios/templateProject/AppDelegate.m')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "#import \\"RNSplashScreen.h\\"
    #import <React/RCTRootView.h>

    - (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
      [self.window makeKeyAndVisible];
    [RNSplashScreen show];
    
      return YES;
    }
    "
  `);
});

it('test images_iOs', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      imagesPath: path.resolve(__dirname, 'splashScreen/images'),
    }
  );

  expect(
    fs.existsSync(
      path.resolve(
        tmpPath,
        `./ios/templateProject/Images.xcassets/bgSplash.imageset/bg.png`
      )
    )
  ).toBeTruthy();

  expect(
    fs.existsSync(
      path.resolve(
        tmpPath,
        `./ios/templateProject/Images.xcassets/logoSplash.imageset/logo.png`
      )
    )
  ).toBeTruthy();
});
