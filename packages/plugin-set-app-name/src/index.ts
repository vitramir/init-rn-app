import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

module.exports = async (
  config: { projectDir: string },
  params: {
    appName: string;
    iosAppName: string;
    androidAppName: string;
  }
) => {
  console.log(`Set app name`);

  const progConf = { base: ".", cwd: config.projectDir };

  const iosAppName = params?.iosAppName || params?.appName;
  if (!iosAppName) {
    throw "iOS app name not provided";
  }
  await Promisify(
    gulp
      .src("./ios/wciom/Info.plist", progConf)
      .pipe(
        inject.replace(
          `<key>CFBundleDisplayName<\\/key>(\\s+)<string>([^<]*)<\\/string>`,
          `<key>CFBundleDisplayName</key>$1<string>${iosAppName}</string>`
        )
      )
      .pipe(gulp.dest("./"))
  );

  const androidAppName = params?.androidAppName || params?.appName;
  if (!androidAppName) {
    throw "Android app name not provided";
  }
  await Promisify(
    gulp
      .src("./android/app/src/main/res/values/strings.xml", progConf)
      .pipe(
        inject.replace(
          `<string name="app_name">(.*)</string>`,
          `<string name="app_name">${androidAppName}</string>`
        )
      )
      .pipe(gulp.dest("./"))
  );

  console.log("Done!");
};
