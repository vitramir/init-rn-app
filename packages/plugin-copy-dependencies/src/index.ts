import * as fs from 'fs';
import * as path from 'path';

module.exports = async (
  config: { projectDir: string },
  params: { package: string }
) => {
  console.log(`Copying dependencies from ${params.package}...`);

  const commonPackageJSON = require(`${params.package}/package.json`);
  const currentPackageJSONPath = path.resolve(
    config.projectDir,
    'package.json'
  );
  let currentPackageJSON = require(currentPackageJSONPath);

  currentPackageJSON = {
    ...currentPackageJSON,
    dependencies: {
      ...currentPackageJSON.dependencies,
      ...commonPackageJSON.dependencies,
    },
  };

  fs.writeFileSync(currentPackageJSONPath, JSON.stringify(currentPackageJSON));

  console.log('Done!');
};
