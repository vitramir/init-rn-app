import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: { path: string; namespace: string }
) => {
  console.log(`Setup custom pods`);

  const progConf = { base: '.', cwd: config.projectDir };

  await Promisify(
    gulp
      .src('./ios/Podfile', progConf)
      .pipe(inject.before('target', `load '${params.path}'\n`))
      .pipe(inject.before('use_native_modules!', `${params.namespace}\n`))
      .pipe(gulp.dest('./'))
  );

  console.log('Done!');
};
