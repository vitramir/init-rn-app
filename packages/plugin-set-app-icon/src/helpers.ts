export const setSizeItem = (item) => {
  if (item.indexOf('@') == -1) return '1024x1024';
  return `${item.slice(item.length - 9, item.indexOf('@'))}x${item.slice(
    item.length - 9,
    item.indexOf('@')
  )}`;
};

export const setScaleItem = (item) => {
  if (item.indexOf('@') == -1) return '1x';
  return item.slice(item.indexOf('@') + 1, item.length - 4);
};

export const setIdiomItem = (item) => {
  if (item.indexOf('@') == -1) return 'ios-marketing';
  return 'iphone';
};
