import Promisify from '@init-rn-app/gulp-promisify';
import { setSizeItem, setIdiomItem, setScaleItem } from './helpers';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor'),
  path = require('path'),
  fs = require('fs'),
  rimraf = require('rimraf'),
  mkdirp = require('mkdirp');

module.exports = async (
  config: {
    projectDir: string;
  },
  params: {
    projectName: string;
    iosIconPath: string;
    androidIconPath: string;
  }
) => {
  console.log('Upload appIcon');

  params.androidIconPath &&
    [
      'mipmap-hdpi',
      'mipmap-mdpi',
      'mipmap-xhdpi',
      'mipmap-xxhdpi',
      'mipmap-xxxhdpi',
    ].forEach(async (dest) => {
      rimraf.sync(`${config.projectDir}/android/app/src/main/res/${dest}`);
      fs.mkdirSync(`${config.projectDir}/android/app/src/main/res/${dest}`);

      fs.copyFileSync(
        `${params.androidIconPath}/${dest}/ic_launcher.png`,
        `${config.projectDir}/android/app/src/main/res/${dest}/ic_launcher.png`
      );

      fs.copyFileSync(
        `${params.androidIconPath}/${dest}/ic_launcher_round.png`,
        `${config.projectDir}/android/app/src/main/res/${dest}/ic_launcher_round.png`
      );

      return null;
    });

  const createIconDirrectory = () => {
    mkdirp.sync(
      `${config.projectDir}/ios/${params.projectName}/Images.xcassets/AppIcon.appiconset`
    );
    seContentsJson();
  };

  const seContentsJson = () => {
    const files = fs.readdirSync(params.iosIconPath, {});
    const nFiles = files.filter(
      (item) => item.slice(item.length - 3, item.length) === 'png'
    );
    if (nFiles.length > 0) {
      nFiles.forEach(async (element) => {
        fs.copyFileSync(
          `${params.iosIconPath}/${element}`,
          `${config.projectDir}/ios/${params.projectName}/Images.xcassets/AppIcon.appiconset/${element}`
        );
      });

      const json =
        `{
        "images" : [` +
        nFiles.map(
          (item) => `{
            "filename" : "${item}",
            "idiom" : "${setIdiomItem(item)}",
            "scale" : "${setScaleItem(item)}",
            "size": "${setSizeItem(item)}"
          }`
        ) +
        `],
        "info" : {
          "version" : 1,
          "author" : "xcode"
        }
      }
      `;
      fs.writeFileSync(
        `${config.projectDir}/ios/${params.projectName}/Images.xcassets/AppIcon.appiconset/Contents.json`,
        json
      );
    }
  };

  if (params.iosIconPath) {
    rimraf.sync(
      `${config.projectDir}/ios/${params.projectName}/Images.xcassets/AppIcon.appiconset`
    );
    createIconDirrectory();
  }

  console.log('Done set appIcon!');
};
