import ncp from 'ncp';
import rimraf from 'rimraf';
import path from 'path';
import fs from 'fs';
import pify from 'pify';
import tmp from 'tmp';

const plugin = require('../src');
const templatePath = path.resolve(__dirname, 'templateProject');
let tmpPath: string;
let tmpObj;

beforeAll(() => {
  tmpObj = tmp.dirSync(undefined);
  tmpPath = tmpObj.name;
});

afterAll(() => {
  rimraf.sync(tmpPath);
});

it('test icon_Android', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      androidIconPath: './packages/plugin-set-app-icon/__tests__/icons/android',
    }
  );

  [
    'mipmap-hdpi',
    'mipmap-mdpi',
    'mipmap-xhdpi',
    'mipmap-xxhdpi',
    'mipmap-xxxhdpi',
  ].forEach((dest) => {
    expect(
      fs.existsSync(
        path.resolve(
          tmpPath,
          `./android/app/src/main/res/${dest}/ic_launcher.png`
        )
      )
    ).toBeTruthy();
  });
});

it('test icon_Android_round', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      androidIconPath: './packages/plugin-set-app-icon/__tests__/icons/android',
    }
  );

  [
    'mipmap-hdpi',
    'mipmap-mdpi',
    'mipmap-xhdpi',
    'mipmap-xxhdpi',
    'mipmap-xxxhdpi',
  ].forEach((dest) => {
    expect(
      fs.existsSync(
        path.resolve(
          tmpPath,
          `./android/app/src/main/res/${dest}/ic_launcher_round.png`
        )
      )
    ).toBeTruthy();
  });
});

it('test iOs Contents.json', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      iosIconPath: './packages/plugin-set-app-icon/__tests__/icons/ios',
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(
          tmpPath,
          './ios/templateProject/Images.xcassets/AppIcon.appiconset/Contents.json'
        )
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "{
            \\"images\\" : [{
                \\"filename\\" : \\"icon-20@2x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"2x\\",
                \\"size\\": \\"20x20\\"
              },{
                \\"filename\\" : \\"icon-20@3x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"3x\\",
                \\"size\\": \\"20x20\\"
              },{
                \\"filename\\" : \\"icon-29@2x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"2x\\",
                \\"size\\": \\"29x29\\"
              },{
                \\"filename\\" : \\"icon-29@3x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"3x\\",
                \\"size\\": \\"29x29\\"
              },{
                \\"filename\\" : \\"icon-40@2x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"2x\\",
                \\"size\\": \\"40x40\\"
              },{
                \\"filename\\" : \\"icon-40@3x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"3x\\",
                \\"size\\": \\"40x40\\"
              },{
                \\"filename\\" : \\"icon-60@2x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"2x\\",
                \\"size\\": \\"60x60\\"
              },{
                \\"filename\\" : \\"icon-60@3x.png\\",
                \\"idiom\\" : \\"iphone\\",
                \\"scale\\" : \\"3x\\",
                \\"size\\": \\"60x60\\"
              },{
                \\"filename\\" : \\"itunesArtwork.png\\",
                \\"idiom\\" : \\"ios-marketing\\",
                \\"scale\\" : \\"1x\\",
                \\"size\\": \\"1024x1024\\"
              }],
            \\"info\\" : {
              \\"version\\" : 1,
              \\"author\\" : \\"xcode\\"
            }
          }
          "
  `);
});

it('test appiconset_iOs', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'templateProject',
      iosIconPath: './packages/plugin-set-app-icon/__tests__/icons/ios',
    }
  );
  const files = fs.readdirSync(
    './packages/plugin-set-app-icon/__tests__/icons/ios',
    {}
  );

  const nFiles = files.filter(
    (item) => item.slice(item.length - 3, item.length) === 'png'
  );

  nFiles.forEach((item) => {
    expect(
      fs.existsSync(
        path.resolve(
          tmpPath,
          `./ios/templateProject/Images.xcassets/AppIcon.appiconset/${item}`
        )
      )
    ).toBeTruthy();
  });
});
