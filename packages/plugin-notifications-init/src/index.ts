import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor"),
  xcode = require("xcode"),
  fs = require("fs");

module.exports = async (
  config: { projectDir: string },
  params: { path: string; namespace: string; projectName: string }
) => {
  console.log(`Setting up notifications...`);

  const progConf = { base: ".", cwd: config.projectDir };

  // ios block start
  console.log("Installing iOS notifications.");
  const entitlementsPath = `${params.projectName}/${params.projectName}.entitlements`;

  //xcode module
  const pbxPath = config.projectDir + "/ios/wciom.xcodeproj/project.pbxproj",
    xcodeProject = xcode.project(pbxPath);

  xcodeProject.parse(function(err) {
    xcodeProject.addFile(
      entitlementsPath,
      xcodeProject.findPBXGroupKey({ name: params.projectName })
    );

    const XCConfigurationListId = xcodeProject.pbxTargetByName(
      params.projectName
    ).buildConfigurationList;

    const BuildConfigurationIds = xcodeProject
      .getPBXGroupByKeyAndType(XCConfigurationListId, "XCConfigurationList")
      .buildConfigurations.map(item => item.value);

    BuildConfigurationIds.forEach(
      id =>
        (xcodeProject.getPBXGroupByKeyAndType(
          id,
          "XCBuildConfiguration"
        ).buildSettings.CODE_SIGN_ENTITLEMENTS = entitlementsPath)
    );

    fs.writeFileSync(pbxPath, xcodeProject.writeSync());
    console.log("Push capability added");
  });

  // gulp module
  await Promisify(
    gulp
      .src(`./ios/${params.projectName}/AppDelegate.m`, progConf)
      .pipe(
        inject.after('#import "AppDelegate.h"', `\n#import "RNNotifications.h"`)
      )
      .pipe(
        inject.after(
          "@implementation AppDelegate",
          `\n
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
  [RNNotifications didRegisterForRemoteNotificationsWithDeviceToken:deviceToken];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
  [RNNotifications didFailToRegisterForRemoteNotificationsWithError:error];
}`
        )
      )
      .pipe(
        inject.after(
          `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{`,
          `\n[RNNotifications startMonitorNotifications];`
        )
      )
      .pipe(gulp.dest("./"))
  );

  // ios block end
  // Andriod block start

  console.log("Installing Android notifications.");
  await Promisify(
    gulp
      .src("./android/app/build.gradle", progConf)
      .pipe(
        inject.after(
          "defaultConfig {",
          `\n\tmissingDimensionStrategy "RNNotifications.reactNativeVersion", "reactNative60"\n`
        )
      )
      .pipe(gulp.dest("./"))
  );
  await Promisify(
    gulp
      .src("./android/app/build.gradle", progConf)
      .pipe(
        inject.after(
          "dependencies {",
          `\n\timplementation 'com.google.firebase:firebase-core:16.0.0'
          implementation "com.google.firebase:firebase-messaging:19.0.0"`
        )
      )
      .pipe(inject.append(`\napply plugin: 'com.google.gms.google-services'`))
      .pipe(gulp.dest("./"))
  );

  console.log("Done!");
};
