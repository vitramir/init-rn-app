import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

module.exports = async (
  config: { projectDir: string },
  params: {
    appVersion: string;
    iosAppVersion: string;
    androidAppVersion: string;
  }
) => {
  console.log(`Set app version`);

  const progConf = { base: ".", cwd: config.projectDir };

  const iosAppVersion = params?.iosAppVersion || params?.appVersion;
  if (!iosAppVersion) {
    throw "iOS app version not provided";
  }
  await Promisify(
    gulp
      .src("./ios/wciom/Info.plist", progConf)
      .pipe(
        inject.replace(
          `<key>CFBundleShortVersionString<\\/key>(\\s+)<string>([^<]*)<\\/string>`,
          `<key>CFBundleShortVersionString</key>$1<string>${iosAppVersion}</string>`
        )
      )
      .pipe(gulp.dest("./"))
  );

  const androidAppVersion = params?.androidAppVersion || params?.appVersion;
  if (!androidAppVersion) {
    throw "Android app version not provided";
  }
  await Promisify(
    gulp
      .src("./android/app/build.gradle", progConf)
      .pipe(
        inject.replace(
          `versionName "1.0"`,
          `versionName "${androidAppVersion}"`
        )
      )
      .pipe(gulp.dest("./"))
  );

  console.log("Done!");
};
