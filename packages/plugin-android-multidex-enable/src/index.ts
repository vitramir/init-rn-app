import Promisify from "@init-rn-app/gulp-promisify";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

module.exports = async (
  config: {
    projectDir: string;
  }
  // params: {
  //   path: string;
  //   namespace: string;
  //   projectName: string;
  // }
) => {
  const progConf = { base: ".", cwd: config.projectDir }; // Project path

  await Promisify(
    gulp
      .src("./android/app/build.gradle", progConf)
      .pipe(inject.after("defaultConfig {", `\nmultiDexEnabled true`))
      .pipe(
        inject.after(
          "dependencies {",
          `\nimplementation 'com.android.support:multidex:1.0.3'`
        )
      )
      .pipe(gulp.dest("./"))
  );

  console.log("Done!");
};
