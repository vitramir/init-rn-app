import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: { permissions: string[] }
) => {
  try {
    console.log(`Setup android permissions`);

    const progConf = { base: '.', cwd: config.projectDir };

    await Promisify(
      gulp
        .src('./android/app/src/main/AndroidManifest.xml', progConf)
        .pipe(
          inject.before(
            '<application',
            `
        ${params.permissions
          .map(
            permission =>
              `<uses-permission android:name="android.permission.${permission}"/>`
          )
          .join('\n')}
        `
          )
        )
        .pipe(gulp.dest('./'))
    );

    console.log('Done!');
  } catch (err) {
    console.log(err);
  }
};
