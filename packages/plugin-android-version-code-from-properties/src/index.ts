import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: { projectName: string; bundleId: string }
) => {
  console.log(`Setup android version code`);

  const progConf = { base: '.', cwd: config.projectDir };

  await Promisify(
    gulp
      .src('./android/app/build.gradle', progConf)
      .pipe(
        inject.after(
          'android {',
          `
      def code=0
      def versionPropsFile = file('version.properties')
      if (!versionPropsFile.canRead()) {
          /*throw new GradleException("Could not read version.properties!")*/
          code = 1
      }
      else{
        def Properties versionProps = new Properties()
        versionProps.load(new FileInputStream(versionPropsFile))
        code = versionProps['VERSION_CODE'].toInteger() + 1
        versionProps['VERSION_CODE']=code.toString()
        versionProps.store(versionPropsFile.newWriter(), null)
      }
    `
        )
      )
      .pipe(inject.replace('versionCode 1', `versionCode code`))
      .pipe(gulp.dest('./'))
  );

  console.log('Done!');
};
