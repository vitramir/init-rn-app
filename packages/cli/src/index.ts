import * as path from 'path';
import * as Bluebird from 'bluebird';

type PackageName = string;
type Params = {};
type PackageNameWithParams = [PackageName, Params];

type Config = {
  presets: (PackageName | PackageNameWithParams)[];
  plugins: (PackageName | PackageNameWithParams)[];
};

export const cli = async () => {
  const root = process.cwd();

  const configFile = require(path.resolve(
    root,
    'init-rn-app.config.js'
  )) as Config;

  const config = {
    projectDir: path.resolve(root, 'rnapp'),
  };

  let plugins: (PackageName | PackageNameWithParams)[] = [];

  if (configFile.presets) {
    await Bluebird.each(configFile.presets, async item => {
      if (typeof item === 'string') {
        const preset = require(item);
        const presetResult = await preset(config);
        if (presetResult && presetResult.plugins) {
          plugins = [...plugins, ...presetResult.plugins];
        }
      } else if (Array.isArray(item)) {
        const [name, params] = item;
        const preset = require(name);
        const presetResult = await preset(config, params);
        if (presetResult && presetResult.plugins) {
          plugins = [...plugins, ...presetResult.plugins];
        }
      }
    });
  }

  if (configFile.plugins) {
    plugins = [...plugins, ...configFile.plugins];
  }

  await Bluebird.each(plugins, async item => {
    if (typeof item === 'string') {
      const plugin = require(item);
      await plugin(config);
    } else if (Array.isArray(item)) {
      const [name, params] = item;
      const plugin = require(name);
      await plugin(config, params);
    }
  });
};
