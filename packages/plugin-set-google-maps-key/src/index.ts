import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: { key: string }
) => {
  console.log(`Set Google Maps key`);

  const progConf = { base: '.', cwd: config.projectDir };

  await Promisify(
    gulp
      .src('./android/app/src/main/AndroidManifest.xml', progConf)

      .pipe(
        inject.before(
          '<activity',
          `
      <meta-data
      android:name="com.google.android.geo.API_KEY"
      android:value="${params.key}"/>

    `
        )
      )
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./ios/**/AppDelegate.m', progConf)
      .pipe(
        inject.before('@implementation AppDelegate', '@import GoogleMaps;\n')
      )
      .pipe(
        inject.after(
          `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions\n{`,
          `
        [GMSServices provideAPIKey:@"${params.key}"];
        `
        )
      )

      .pipe(gulp.dest('./'))
  );

  console.log('Done!');
};
