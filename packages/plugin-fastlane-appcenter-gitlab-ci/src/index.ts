import * as path from 'path';

import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor'),
  debug = require('gulp-debug');

module.exports = async (
  config: { projectDir: string },
  params: {
    bundleId: string;
    projectName: string;
    android: {
      bundleId?: string;
      appcenterOwnerName: string;
      appcenterAppName: string;
      destinations: string;
    };
    ios: {
      bundleId?: string;
      appcenterOwnerName: string;
      appcenterAppName: string;
      destinations: string;
      appcenterAppleID: string;
      appcenterAppleTeamID: string;
      appcenterAppleITCTeamID: string;
      appcenterAppleCodeSigningIdentity: string;
      appcenterAppleKeysGit: string;
    };
  }
) => {
  console.log(`Setup fastlane`);

  const progConf = {
    base: path.resolve(__dirname, '../template/'),
    cwd: config.projectDir,
  };

  await Promisify(
    gulp
      .src(path.resolve(__dirname, '../template/**/*'), progConf)
      .pipe(gulp.dest(config.projectDir))
  );

  const androidBundleId = params?.android?.bundleId || params?.bundleId;
  await Promisify(
    gulp
      .src(path.resolve(__dirname, '../template/android/fastlane/*'), progConf)
      .pipe(inject.replace('BUNDLE_ID', androidBundleId))
      .pipe(
        inject.replace(
          'APPCENTER_OWNER_NAME',
          params.android.appcenterOwnerName
        )
      )
      .pipe(
        inject.replace('APPCENTER_APP_NAME', params.android.appcenterAppName)
      )
      .pipe(
        inject.replace('APPCENTER_DESTINATIONS', params.android.destinations)
      )
      .pipe(gulp.dest(config.projectDir))
  );

  const iosBundleId = params?.ios?.bundleId || params?.bundleId;
  await Promisify(
    gulp
      .src(path.resolve(__dirname, '../template/ios/fastlane/*'), progConf)
      .pipe(inject.replace('BUNDLE_ID', iosBundleId))
      .pipe(inject.replace('PROJECT_NAME', params.projectName))
      .pipe(
        inject.replace('APPCENTER_OWNER_NAME', params.ios.appcenterOwnerName)
      )
      .pipe(inject.replace('APPCENTER_APP_NAME', params.ios.appcenterAppName))
      .pipe(inject.replace('APPCENTER_DESTINATIONS', params.ios.destinations))
      .pipe(inject.replace('APPCENTER_APPLE_ID', params.ios.appcenterAppleID))
      .pipe(
        inject.replace(
          'APPCENTER_APPLE_TEAM_ID',
          params.ios.appcenterAppleTeamID
        )
      )
      .pipe(
        inject.replace(
          'APPCENTER_APPLE_ITC_TEAM_ID',
          params.ios.appcenterAppleITCTeamID
        )
      )
      .pipe(
        inject.replace(
          'APPCENTER_APPLE_CODE_SIGNING_IDENTITY',
          params.ios.appcenterAppleCodeSigningIdentity
        )
      )
      .pipe(
        inject.replace(
          'FASTLANE_APPLE_KEYS_GIT',
          params.ios.appcenterAppleKeysGit
        )
      )
      .pipe(gulp.dest(config.projectDir))
  );

  console.log('Done!');
};
