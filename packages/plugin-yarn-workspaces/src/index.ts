import Promisify from '@init-rn-app/gulp-promisify';
import * as path from 'path';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (config: { projectDir: string }) => {
  console.log(`Updating paths for yarn workspaces`);

  const progConf = { base: '.', cwd: config.projectDir };

  await Promisify(
    gulp
      .src(path.resolve(__dirname, '../template/**/*'), {
        base: path.resolve(__dirname, '../template/'),
      })
      .pipe(gulp.dest(config.projectDir))
  );

  await Promisify(
    gulp
      .src('./android/settings.gradle', progConf)
      .pipe(
        inject.replace(
          'file\\("\\.\\.\\/node_modules\\/@react-native-community\\/cli-platform-android\\/native_modules.gradle"\\)',
          'file("./native_modules.gradle")'
        )
      )
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./ios/Podfile', progConf)
      .pipe(inject.replace('../node_modules', '../../../../node_modules'))
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./ios/**/project.pbxproj', progConf)
      .pipe(inject.replace('../node_modules', '../../../../node_modules'))
      .pipe(
        inject.before(
          'export NODE_BINARY=node',
          'export PROJECT_ROOT=$(pwd)/..\n'
        )
      )
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./android/settings.gradle', progConf)
      .pipe(inject.replace('../node_modules', '../../../../node_modules'))
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./android/app/build.gradle', progConf)
      .pipe(inject.replace('../node_modules', '../../../../node_modules'))
      .pipe(
        inject.afterEach(
          'project.ext.react = [',
          '\ncliPath:"node_modules/.bin/react-native",'
        )
      )
      .pipe(gulp.dest('./'))
  );

  await Promisify(
    gulp
      .src('./android/build.gradle', progConf)
      .pipe(inject.replace('../node_modules', '../../../../node_modules'))
      .pipe(gulp.dest('./'))
  );

  console.log('Done!');
};
