import ncp from 'ncp';
import rimraf from 'rimraf';
import path from 'path';
import fs from 'fs';
import pify from 'pify';
import tmp from 'tmp';

const plugin = require('../src');
const templatePath = path.resolve(__dirname, 'templateProject');
let tmpPath: string;
let tmpObj;

beforeAll(() => {
  tmpObj = tmp.dirSync(undefined);
  tmpPath = tmpObj.name;
});

afterAll(() => {
  rimraf.sync(tmpPath);
});

it('test pluginForAndroid_cursor_only', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      cursorColor: {
        hex: '#e94e1b',
      },
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/res/values/styles.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<resources>

        <!-- Base application theme. -->
        <style name=\\"AppTheme\\" parent=\\"Theme.AppCompat.Light.NoActionBar\\">
              <!-- sets cursor color -->
              <item name=\\"colorControlActivated\\">#e94e1b</item>
            <!-- Customize your theme here. -->
            <item name=\\"android:textColor\\">#000000</item>
        </style>

    </resources>
    "
  `);
});

it('test pluginForAndroid_tint_only', async () => {
  rimraf.sync(tmpPath);
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      tintColor: {
        hex: '#e94e1b',
      },
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/res/values/styles.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<resources>

        <!-- Base application theme. -->
        <style name=\\"AppTheme\\" parent=\\"Theme.AppCompat.Light.NoActionBar\\">
              <!-- sets active component color -->
              <item name=\\"colorAccent\\">#e94e1b</item>
            <!-- Customize your theme here. -->
            <item name=\\"android:textColor\\">#000000</item>
        </style>

    </resources>
    "
  `);
});

it('test pluginForAndroid_all', async () => {
  rimraf.sync(tmpPath);
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      cursorColor: {
        hex: '#e94e1b',
      },
      tintColor: {
        hex: '#e94e1b',
      },
    }
  );

  expect(
    fs
      .readFileSync(
        path.resolve(tmpPath, './android/app/src/main/res/values/styles.xml')
      )
      .toString()
  ).toMatchInlineSnapshot(`
    "<resources>

        <!-- Base application theme. -->
        <style name=\\"AppTheme\\" parent=\\"Theme.AppCompat.Light.NoActionBar\\">
              <!-- sets active component color -->
              <item name=\\"colorAccent\\">#e94e1b</item>
              <!-- sets cursor color -->
              <item name=\\"colorControlActivated\\">#e94e1b</item>
            <!-- Customize your theme here. -->
            <item name=\\"android:textColor\\">#000000</item>
        </style>

    </resources>
    "
  `);
});

it('test pluginForIOS_cursor_only', async () => {
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  //apply plugin
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      cursorColor: {
        rgba: { r: '255.0', g: '255.0', b: '255.0', a: 1.0 },
      },
    }
  );

  expect(
    fs
      .readFileSync(path.resolve(tmpPath, './ios/rnapp/AppDelegate.m'))
      .toString()
  ).toMatchInlineSnapshot(`
    "- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
      [[UITextField appearance] setTintColor:[[UIColor alloc] initWithRed:255.0 green:255.0 blue:255.0 alpha:1]];

      [self.window makeKeyAndVisible];
      return YES;
    }
    "
  `);
});

it('test pluginForIOS_tint_only', async () => {
  rimraf.sync(tmpPath);
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  //apply plugin
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      tintColor: {
        rgba: { r: '235.5/255.0', g: '78.5/255.0', b: '27.5/255.0', a: 0.8 },
      },
    }
  );

  expect(
    fs
      .readFileSync(path.resolve(tmpPath, './ios/rnapp/AppDelegate.m'))
      .toString()
  ).toMatchInlineSnapshot(`
    "- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
      [[UIView appearance] setTintColor:[[UIColor alloc] initWithRed:235.5/255.0 green:78.5/255.0 blue:27.5/255.0 alpha:0.8]];

      [self.window makeKeyAndVisible];
      return YES;
    }
    "
  `);
});

it('test pluginForIOS_all', async () => {
  rimraf.sync(tmpPath);
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  //apply plugin
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      cursorColor: {
        rgba: { r: '233.0/255.0', g: '78.0/255.0', b: '27.0/255.0', a: 1.0 },
      },
      tintColor: {
        rgba: { r: '233.0/255.0', g: '78.0/255.0', b: '27.0/255.0', a: 1.0 },
      },
    }
  );

  expect(
    fs
      .readFileSync(path.resolve(tmpPath, './ios/rnapp/AppDelegate.m'))
      .toString()
  ).toMatchInlineSnapshot(`
    "- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
    {
      [[UITextField appearance] setTintColor:[[UIColor alloc] initWithRed:233.0/255.0 green:78.0/255.0 blue:27.0/255.0 alpha:1]];

      [[UIView appearance] setTintColor:[[UIColor alloc] initWithRed:233.0/255.0 green:78.0/255.0 blue:27.0/255.0 alpha:1]];

      [self.window makeKeyAndVisible];
      return YES;
    }
    "
  `);
});
