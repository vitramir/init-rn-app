import Promisify from '@init-rn-app/gulp-promisify';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: {
    cursorColor: {
      hex: string;
      rgba: { r: string; g: string; b: string; a: number };
    };
    tintColor: {
      hex: string;
      rgba: { r: string; g: string; b: string; a: number };
    };
  }
) => {
  console.log(`Set Cursor&Tint Color`);

  const progConf = { base: '.', cwd: config.projectDir };

  const iosCursorColor = params?.cursorColor?.rgba;
  const iosTintColor = params?.tintColor?.rgba;
  const androidCursorColor = params?.cursorColor?.hex;
  const androidTintColor = params?.tintColor?.hex;

  if (androidCursorColor) {
    await Promisify(
      gulp
        .src('./android/app/src/main/res/values/styles.xml', progConf)

        .pipe(
          inject.after(
            ' <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">',
            `
          <!-- sets cursor color -->
          <item name="colorControlActivated">${androidCursorColor}</item>`
          )
        )
        .pipe(gulp.dest('./'))
    );
  }

  if (androidTintColor) {
    await Promisify(
      gulp
        .src('./android/app/src/main/res/values/styles.xml', progConf)

        .pipe(
          inject.after(
            ' <style name="AppTheme" parent="Theme.AppCompat.Light.NoActionBar">',
            `
          <!-- sets active component color -->
          <item name="colorAccent">${androidTintColor}</item>`
          )
        )
        .pipe(gulp.dest('./'))
    );
  }

  if (iosTintColor) {
    await Promisify(
      gulp
        .src('./ios/**/AppDelegate.m', progConf)
        .pipe(
          inject.after(
            `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions\n{\n`,
            `  [[UIView appearance] setTintColor:[[UIColor alloc] initWithRed:${iosTintColor.r} green:${iosTintColor.g} blue:${iosTintColor.b} alpha:${iosTintColor.a}]];\n\n`
          )
        )

        .pipe(gulp.dest('./'))
    );
  }

  if (iosCursorColor) {
    await Promisify(
      gulp
        .src('./ios/**/AppDelegate.m', progConf)
        .pipe(
          inject.after(
            `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions\n{\n`,
            `  [[UITextField appearance] setTintColor:[[UIColor alloc] initWithRed:${iosCursorColor.r} green:${iosCursorColor.g} blue:${iosCursorColor.b} alpha:${iosCursorColor.a}]];\n\n`
          )
        )

        .pipe(gulp.dest('./'))
    );
  }

  console.log('Done set cursor&tint color!');
};
