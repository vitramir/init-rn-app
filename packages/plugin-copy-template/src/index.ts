import * as path from 'path';
import copyProjectTemplateAndReplace from '@react-native-community/cli/build/tools/generator/copyProjectTemplateAndReplace';

module.exports = async (
  config: { projectDir: string },
  params: { template: string; projectName: string }
) => {
  console.log(`Copying template ${params.template}...`);

  const templatePath = path.dirname(
    require.resolve(`${params.template}/template`)
  );

  await copyProjectTemplateAndReplace(
    templatePath,
    config.projectDir,
    params.projectName
  );

  console.log('Done!');
};
