import Promisify from "@init-rn-app/gulp-promisify";
import * as path from "path";

const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

module.exports = async (
  config: { projectDir: string },
  params: {
    appVersion: string;
    iosAppVersion: string;
    androidAppVersion: string;
  }
) => {
  console.log(`Copying files`);

  const progConf = {
    base: path.resolve(process.cwd(), "init-rn-app-copy-files"),
    cwd: config.projectDir
  };

  await Promisify(
    gulp
      .src(path.resolve(process.cwd(), "init-rn-app-copy-files/**/*"), progConf)
      .pipe(gulp.dest(config.projectDir))
  );

  console.log("Done!");
};
