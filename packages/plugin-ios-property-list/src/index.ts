import Promisify from '@init-rn-app/gulp-promisify';
import path from 'path';
import plist from 'plist';
import fs from 'fs';
import R from 'ramda';

const gulp = require('gulp'),
  rename = require('gulp-rename'),
  inject = require('gulp-inject-string'),
  jsonedit = require('gulp-json-editor');

module.exports = async (
  config: { projectDir: string },
  params: {
    projectName: string;
    properties: object;
  }
) => {
  console.log(`Setup iOs properties`);

  const iosAppName = params?.projectName;
  const iosProperties = params?.properties;
  const pathPlist = path.resolve(
    config.projectDir,
    `./ios/${iosAppName}/Info.plist`
  );

  const objDefault = plist.parse(fs.readFileSync(pathPlist, 'utf8'));
  const mergePlist = R.pipe(R.merge(objDefault), plist.build);
  fs.writeFileSync(pathPlist, mergePlist(iosProperties));

  console.log('Done add ios properties!');
};
