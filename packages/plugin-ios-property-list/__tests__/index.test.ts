import ncp from 'ncp';
import rimraf from 'rimraf';
import path from 'path';
import fs from 'fs';
import pify from 'pify';
import plist from 'plist';

const plugin = require('../src');

it('test plugin', async () => {
  const templatePath = path.resolve(__dirname, 'templateProject');
  const tmpPath = path.resolve(__dirname, '_tmp');

  rimraf.sync(tmpPath); //remove tmp dir
  await pify(ncp)(templatePath, tmpPath); //clone template to tmp dir

  //apply plugin
  await plugin(
    {
      projectDir: tmpPath,
    },
    {
      projectName: 'app',
      properties: {
        CFBundleDevelopmentRegion: 'ru',
      },
    }
  );

  const result = plist.parse(
    fs.readFileSync(path.resolve(tmpPath, './ios/app/Info.plist'), 'utf8')
  );

  expect(result).toEqual({
    CFBundleDevelopmentRegion: 'ru',
    CFBundleDisplayName: 'app',
    UILaunchStoryboardName: 'LaunchScreen',
    UIViewControllerBasedStatusBarAppearance: false,
  });
});
