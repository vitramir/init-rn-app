# Working with init-rn-app

## Installation

Create new directory:  
```bash
mkdir init-rn-app-example && cd init-rn-app-example
```

InItialize new package:  
```bash
yarn init -y
```

Edit `package.json` to setup workspaces:   
```
"private":true,
"workspaces": {
    "packages": [
      "packages/*",
      "packages/**/rnapp"
    ]
  }
```

Create new package:  
```bash
mkdir packages && cd packages
mkdir sample-app && cd sample-app
yarn init -y
```

Install dependencies (React Native and init-rn-app with plugins):

```bash
yarn add -D react@16.8.1 react-native@0.61.2 rimraf @init-rn-app/cli @init-rn-app/plugin-copy-template @init-rn-app/plugin-set-bundle-id @init-rn-app/plugin-yarn-workspaces
```

Edit `package.json`  to add project generation script

```
"scripts": {
    "gen": "rimraf rnapp && yarn init-rn-app"
}
```


Add `init-rn-app.config.js`. You can define plugins and presets with supported parameters in this file.

```
module.exports = {
  plugins: [
    [
      "@init-rn-app/plugin-copy-template",
      { template: "react-native", projectName: "sampleApp" },
    ],
	  "@init-rn-app/plugin-yarn-workspaces",
    [
      "@init-rn-app/plugin-set-bundle-id",
      {
        projectName: "sampleApp",
        bundleId: `com.test.sampleApp`,
      },
    ],
  ],
};
```

Add  `index.js`. We export app from rnapp for this showcase, but you can use it as entry point for your code. 
```
import App from "./rnapp/App";

export default App;
```

We are ready to generate project now:

```bash
yarn gen && yarn install
cd rnapp/ios && pod install && cd ../
```

Run Metro bundler:
`yarn start`

And run app from another terminal window:
`npx react-native run-ios`

Repo with this sample can be found here: [init-rn-app-example](https://gitlab.com/vitramir/init-rn-app-example)


## Initializing new plugin

1. Run `node init-new-plugin.js` at root directory
2. Input plugin name
3. Done!

## Editing plugin

1. Navigate to plugin folder
2. Edit lib/index.ts
3. Compile plugin with `yarn tsc` (you may use `--watch` option)
4. Done!

## Running at project

1. If running plugin locally: run `yarn link` at plugin folder. Then run `yarn link @init-rn-app/[your plugin name]` at project root folder
2. Otherwise run `yarn add @init-rn-app/[your-plugin-name]`
3. Add to 'plugins' array of `initrnapp-preset/src/index.ts` your plugin with the following syntax:

```javascript
["your-plugin-name", { ...yourParamsHere }];
```

4. Done!
