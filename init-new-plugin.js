const fs = require("fs-extra");
const gulp = require("gulp"),
  rename = require("gulp-rename"),
  inject = require("gulp-inject-string"),
  jsonedit = require("gulp-json-editor");

const readline = require("readline").createInterface({
  input: process.stdin,
  output: process.stdout
});

readline.question("Input plugin name \n", path => {
  fs.copySync(`./blank-plugin`, `./packages/${path}`);

  const data = fs
    .readFileSync(`./packages/${path}/package.json`)
    .toString("utf-8")
    .replace("pluginNameHere", path);

  fs.writeFileSync(
    `./packages/${path}/package.json`,

    data,
    err => {
      if (err) throw err;
    }
  );
  readline.close();
});
